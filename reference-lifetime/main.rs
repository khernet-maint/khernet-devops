// Make a constant with `'static` lifetime.
static NUM: i32 = 18;

// Returns a reference to `NUM` where its `'static`
// lifetime is coerced to that of the input argument.
fn coerce_static<'a>(_: &'a i32) -> &'a i32 
{
    &NUM
}

fn main() 
{
    {
        // Make a `string` literal and print it:
        let static_string = "I'm in read-only memory";
        println!("static_string: {}", static_string);

        // When `static_string` goes out of scope, the reference
        // can no longer be used, but the data remains in the binary.
    }

    // This line fails
    // error[E0425]: cannot find value `static_string` in this scope
    //println!("static_string: {}", static_string);

    {
        // Make an integer to use for `coerce_static`:
        let lifetime_num = 9;

        // Coerce `NUM` to lifetime of `lifetime_num`:
        let coerced_static = coerce_static(&lifetime_num);

        println!("coerced_static: {}", coerced_static);

        let newVar=coerced_static;

        println!("newVar value={}",newVar);
        println!("again coerced_static value={}",coerced_static);
    }

    println!("NUM: {} stays accessible!", NUM);
}
