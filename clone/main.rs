// Differs from Copy in that Copy is implicit and extremely inexpensive, 
// while Clone is always explicit and may or may not be expensive. 
// In order to enforce these characteristics, Rust does not allow you to reimplement Copy, 
// but you may reimplement Clone and run arbitrary code.
// Since Clone is more general than Copy, 
// you can automatically make anything Copy be Clone as well.

// A unit struct without resources
#[derive(Debug, Clone, Copy)]
struct Unit;

//#[derive(Debug, Clone, Copy)]
#[derive(Debug)]
struct Unit2
{
    candy:i32,
}


// A tuple struct with resources that implements the `Clone` trait
#[derive(Clone, Debug)]
struct Pair(Box<i32>, Box<i32>);

fn main() 
{
    // Instantiate `Unit`
    let unit = Unit;
    // Copy `Unit`, there are no resources to move
    let copied_unit = unit;

    // Both `Unit`s can be used independently
    println!("original: {:?}", unit);
    println!("copy: {:?}", copied_unit);

    // Instantiate `Pair`
    let pair = Pair(Box::new(1), Box::new(2));
    println!("original: {:?}", pair);

    // Move `pair` into `moved_pair`, moves resources
    let moved_pair = pair;
    println!("moved: {:?}", moved_pair);

    // Error! `pair` has lost its resources
    //println!("original: {:?}", pair);
    // TODO ^ Try uncommenting this line

    // Clone `moved_pair` into `cloned_pair` (resources are included)
    let cloned_pair = moved_pair.clone();
    // Drop the original pair using std::mem::drop
    drop(moved_pair);

    // Error! `moved_pair` has been dropped
    //println!("copy: {:?}", moved_pair);
    // TODO ^ Try uncommenting this line

    // The result from .clone() can still be used!
    println!("clone: {:?}", cloned_pair);

    let candy1=Unit2{candy:3};

    let candy_copy=candy1;

    // candy1 cannot be used because it was moved in the above line
    // Unit2 must implement Copy trait for this line to work
    //println!("Original candy1 = {:?}",candy1);
    println!("Copy candy1 = {:?}",candy_copy);
}
