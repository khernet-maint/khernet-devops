use std::ops::{Add, Mul, Sub};

macro_rules! assert_equal_len 
{
    // The `tt` (token tree) designator is used for
    // operators and tokens.
    ($a:expr, $b:expr, $func:ident, $op:tt) => 
    {
        assert!($a.len() == $b.len(),
                "{:?}: dimension mismatch: {:?} {:?} {:?}",
                stringify!($func),
                ($a.len(),),
                stringify!($op),
                ($b.len(),));
    };
}

macro_rules! math_operations
{
    ($function_name:ident, $operation_trait:ident, $operation_symbol:tt, $actual_operation:ident) => 
    {
        fn $function_name<T: $operation_trait<T, Output=T> + Copy>(xs: &mut Vec<T>, ys: &Vec<T>) 
        {
            assert_equal_len!(xs, ys, $function_name, $operation_symbol);

            for (x, y) in xs.iter_mut().zip(ys.iter()) 
            {
                *x = $operation_trait::$actual_operation(*x, *y);
                // *x = x.$method(*y);
            }
        }
    };
}

// Implement `add_assignooooo`, `add_assignooooo`, and `add_assignooooo` functions.
math_operations!(add_assignooooo, Add, +=, add);// `add` is a function of `Add` trait
math_operations!(mul_assignooooo, Mul, *=, mul);// `mul` is a function of `Mul` trait
math_operations!(sub_assignooooo, Sub, -=, sub);// `sub` is a function of `Sub` trait

mod test 
{
    use std::iter;
    macro_rules! test 
    {
        ($func1:ident, $x:expr, $y:expr, $z:expr) => 
        {
            #[test]
            fn $func1() 
            {
                // Run math_operations ten times
                for size in 0usize..10
                {
                    // `repeat()` creates an iterator that endlessly repeats a single element
                    // In the example below the value `$x` is repeated over and over (`next()` can be called to iterate)

                    // `take()` creates an iterator that yields the first `n` elements

                    // `collect()` transforms and iterator into a collection
                    // You can take a collection, call iter on it, do a bunch of transformations, and then collect() at the end.

                    let mut x: Vec<_> = iter::repeat($x).take(size).collect();
                    let y: Vec<_> = iter::repeat($y).take(size).collect();
                    let z: Vec<_> = iter::repeat($z).take(size).collect();

                    // Call the function implemented with `math_operations` macro
                    super::$func1(&mut x, &y);

                    // This line will call the `$func1` test function
                    //$func1(&mut x, &y);

                    assert_eq!(x, z);
                }
            }
        };
    }

    // Test `add_assign`, `mul_assign`, and `sub_assign`.

    // create vectors of any size with the numbers specifying in the last 3 parameters, for example:
    // First vector [1, 1, 1]
    // Second vector [2, 2, 2]
    // Third vector [3, 3, 3]
    test!(add_assignooooo, 1u32, 2u32, 3u32);// last parameter is the result of adding first and second parameter: 1 + 2 = 3
    test!(mul_assignooooo, 2u32, 3u32, 6u32);// last parameter is the result of multiplying first and second parameter: 2 * 3 = 6
    test!(sub_assignooooo, 3u32, 2u32, 1u32);// last parameter is the result of subtracting first and second parameter: 3 - 2 = 1
}
