// `test!` will compare `$left` and `$right`
// in different ways depending on how you invoke it:
macro_rules! test 
{
    // Arguments don't need to be separated by a comma.
    // Any template can be used!
    ($left:expr; and $right:expr) => 
    {
        println!("{:?} and {:?} is {:?}",
                 stringify!($left),
                 stringify!($right),
                 $left && $right)
    };
    // ^ each arm must end with a semicolon.
    ($left:expr; or $right:expr) => 
    {
        println!("{:?} or {:?} is {:?}",
                 stringify!($left),
                 stringify!($right),
                 $left || $right)
    };

    ($first_expr:expr, and second_expr:expr or third_expr:expr) =>
    {
        println!("First expression:\n{:?} Second expression:\n{:?} Third expression:\n{:?}",// Result: {:?}",
                stringify!($first_expr),
                stringify!($second_expr),
                stringify!($third_expr))
                //,
                //$first_expr&&$second_expr&&$third_expr)
    }
}

fn main() 
{
    test!(1i32 + 1 == 2i32; and 2i32 * 2 == 4i32);
    test!(true; or false);

    test!(3.2f32==4f32; and  1-1==0 && 349i64==349i64);
}
