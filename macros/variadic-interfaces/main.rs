// A variadic interface takes an arbitrary number of arguments. 
// For example, println! can take an arbitrary number of arguments

macro_rules! calculate 
{
    // The pattern for a single `eval`
    (eval $e:expr) => 
    {{
        {
            let val: usize = $e; // Force types to be integers
            println!("{} = {}", stringify!{$e}, val);
        }
    }};

    // Decompose multiple `eval`s recursively
    (eval $e:expr, $(eval $es:expr),+) => 
    {{
        calculate! { eval $e }
        calculate! { $(eval $es),+ }
    }};

    (evaluar $e:expr, $(eval $es:expr),*) => 
    {{
        calculate! { eval $e }
        calculate! { $(eval $es),+ }
    }};
}

fn main() 
{
    calculate! { // Look ma! Variadic `calculate!`!
        eval 1 + 2,
        eval 3 + 4,
        eval (2 * 3) + 1,
        eval  1 * 9 + 6,
        eval 4*(10-3)/3
    }

    // More than a "keyword" (`eval`, `evaluar`) can be used into a macro
    calculate! { // Look ma! Variadic `calculate!`!
        evaluar 44 + 8,
        eval 2 + 9,
        eval (0 * 3) + 1,
        eval  54 * 9 + 6,
        eval 4*(10-3)/3
    }
}
