// A DSL is a mini "language" embedded in a Rust macro. 
// It is completely valid Rust because the macro system expands into normal Rust constructs,
// but it looks like a small language. 
// This allows you to define concise or intuitive syntax for some special functionality (within bounds).

macro_rules! calculate 
{
    // Note the two pairs of braces in the macro. 
    // The outer ones are part of the syntax of macro_rules!, in addition to () or [].

    (evaluar $e:expr) => 
    {{
        {
            let val: usize = $e; // Force types to be integers
            println!("{} = {}", stringify!{$e}, val);
        }
    }};
}

fn main() 
{
    calculate! 
    {
        evaluar 1 + 2 // hehehe `evaluar` is _not_ a Rust keyword!
    }

    calculate! 
    {
        evaluar (1 + 2) * (3 / 4)
    }
}
