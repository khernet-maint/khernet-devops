fn drink(beverage: &str) 
{
    // You shouldn't drink too much sugary beverages.
    if beverage == "lemonade" { panic!("AAAaaaaa!!!!"); }

    println!("Some refreshing {} is all I need.", beverage);
}

fn main() 
{
    drink("water");
    drink("lemonade");

    // Output:
    // Some refreshing water is all I need.
    // thread 'main' panicked at 'AAAaaaaa!!!!', main.rs:4:33
    // note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace

}

// On windows execute the following if you want to print backtrace (stacktrace)
// If you're using cmd, it's: set RUST_BACKTRACE=1
// If you're using powershell, it's $Env:RUST_BACKTRACE=1
// It's just a regular environment variable.