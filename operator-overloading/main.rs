use std::ops;

struct Foo;
struct Bar;

#[derive(Debug)]
struct FooBar;

#[derive(Debug)]
struct BarFoo;

// The `std::ops::Add` trait is used to specify the functionality of `+`.
// Here, we make `Add<Bar>` - the trait for addition with a RHS of type `Bar`.
// The following block implements the operation: Foo + Bar = FooBar
impl ops::Add<Bar> for Foo 
{
    type Output = FooBar;

    fn add(self, _rhs: Bar) -> FooBar 
    {
        println!("> Foo.add(Bar) was called");

        FooBar
    }
}

// By reversing the types, we end up implementing non-commutative addition.
// Here, we make `Add<Foo>` - the trait for addition with a RHS of type `Foo`.
// This block implements the operation: Bar + Foo = BarFoo
impl ops::Add<Foo> for Bar 
{
    // Included in Add trait
    type Output = BarFoo;

    fn add(self, _rhs: Foo) -> BarFoo 
    {
        println!("> Bar.add(Foo) was called");

        BarFoo
    }
}

impl ops::Add<Bar> for BarFoo
{
    type Output = BarFoo;

    fn add(self, _rhs: Bar)->BarFoo
    {
        println!("> BarFoo.add(Bar) was called");

        BarFoo
    }

}

fn main() 
{
    // In Rust, many of the operators can be overloaded via traits. 
    // That is, some operators can be used to accomplish different tasks 
    // based on their input arguments. 
    // This is possible because operators are syntactic sugar for method calls. 

    println!("Foo + Bar = {:?}", Foo + Bar);
    println!("Bar + Foo = {:?}", Bar + Foo);
    println!("Bar + Foo + Bar = {:?}", Bar + Foo+Bar);
}
