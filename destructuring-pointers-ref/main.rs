﻿fn main() 
{
    // Assign a reference of type `i32`. The `&` signifies there
    // is a reference being assigned.
    //let reference = &423.11;
    let reference = &34;

    // All these three lines prints the same
    // The reference is 34
    println!("The reference is {}",reference);
    println!("The reference is {}",&reference);
    println!("The reference is {}",*reference);

    match reference 
    {
        // If `reference` is pattern matched against `&val`, it results
        // in a comparison like:
        // `&i32`
        // `&val`
        // ^ We see that if the matching `&`s are dropped, then the `i32`
        // should be assigned to `val`.
        &valor => println!("Got a value via destructuring: {:?}", valor),// prints the `reference` real value
    }

    // To avoid the `&`, you dereference before matching.
    match *reference 
    {
        valor1 => println!("Got a value via dereferencing: {:?}", valor1),// prints the `reference` real value
    }

    match &reference 
    {
        valor => println!("Got a value for..: {:?}", valor),// prints the `reference` real value
    }

    match reference 
    {
        34 => println!("A new value has come: 34"),
        valor2 => println!("Got a value (other way)..: {:?}", valor2),// prints the `reference` real value
    }

    match reference 
    {
        &valor2 => println!("Got a value with &..: {:?}", &valor2),// prints the `reference` real value
    }

    // expected pattern, found `*`
    // match reference 
    // {
    //     *valor2 => println!("Got a value (other way)..: {:?}", *valor2),
    // }

    // What if you don't start with a reference? `reference` was a `&`
    // because the right side was already a reference. This is not
    // a reference because the right side is not one.
    let _not_a_reference = 3;

    // Rust provides `ref` for exactly this purpose. It modifies the
    // assignment so that a reference is created for the element; this
    // reference is assigned.
    let ref _is_a_reference = 3;

    // Accordingly, by defining 2 values without references, references
    // can be retrieved via `ref` and `ref mut`.
    let value = 5;
    let mut mut_value = 6;

    // Use `ref` keyword to create a reference.
    match value 
    {
        9=> println!("9 value found"),
        ref r => println!("Got a reference to a value: {:?}", r),
    }

    // Use `ref mut` similarly.
    match mut_value 
    {
        ref mut m => {
            // Got a reference. Gotta dereference it before we can
            // add anything to it.
            *m += 10;
            println!("We added 10. `mut_value`: {:?}", m);
        },
    }

    match mut_value 
    {
        ref mut m => {
            // Got a reference. Gotta dereference it before we can
            // add anything to it.
            *m += 10;
            println!("We added 10. `mut_value`: {:?}", m);
        },
    }
}
