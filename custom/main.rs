﻿#[cfg(aqui_mi_condition)]
fn conditional_function() 
{
    println!("condition met!");
}

fn main() 
{
    // On Windows when run this command
    // rustc main.rs
    // The following is raised
    // error[E0425]: cannot find function `conditional_function` in this scope

    // Run this to compile adding `aqui_mi_condition` conditional
    // rustc --cfg some_condition main.rs
    conditional_function();
}
