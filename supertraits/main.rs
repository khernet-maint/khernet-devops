struct Human
{
    first_name:String,
}

trait Person 
{
    fn name(&self) -> String;
}

// Person is a supertrait of Student.
// Implementing Student requires you to also impl Person.
trait Student: Person 
{
    fn university(&self) -> String;
}

trait Programmer 
{
    fn fav_language(&self) -> String;
}

// CompSciStudent (computer science student) is a subtrait of both Programmer 
// and Student. Implementing CompSciStudent requires you to impl both supertraits.
trait CompSciStudent: Programmer + Student 
{
    fn git_username(&self) -> String;
}

impl Programmer for Human
{
    fn fav_language(&self) -> String
    {
        "Rust".to_string()
    }
}

impl Student for Human
{
    fn university(&self) -> String
    {
        "Latam".to_string()
    }
}

impl Person for Human
{
    fn name(&self) -> String
    {
        // This line also works
        //self.first_name.clone()

        (*self.first_name).to_string()
    }
}

impl CompSciStudent for Human
{
    fn git_username(&self) -> String
    {
        "lemalcs".to_string()
    }
}

fn comp_sci_student_greeting(student: &dyn CompSciStudent) -> String 
{
    format!(
        "My name is {} and I attend {}. My favorite language is {}. My Git username is {}",
        student.name(),
        student.university(),
        student.fav_language(),
        student.git_username()
    )
}

fn main() 
{
    let programmer=Human{first_name:"Luis".to_string()};
    println!("{}",comp_sci_student_greeting(&programmer));
}
