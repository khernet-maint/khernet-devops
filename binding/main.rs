﻿// A function `age` which returns a `u32`.
fn age() -> u32 
{
    32
}

//Option is a native enum
fn some_number() -> Option<u32> {

    // Some is a native tuple struct
    Some(423)
}

fn main() 
{
    println!("Tell me what type of person you are");

    // Assign the result of a function to a variable `n`
    // so it can be used latter
    match age() 
    {
        0             => println!("I haven't celebrated my first birthday yet"),
        // Could `match` 1 ..= 12 directly but then what age
        // would the child be? Instead, bind to `n` for the
        // sequence of 1 ..= 12. Now the age can be reported.
        n @ 1  ..= 12 => println!("I'm a child of age {:?}", n),
        n @ 13 ..= 19 => println!("I'm a teen of age {:?}", n),
        // Nothing bound. Return the result.
        n             => println!("I'm an old person of age {:?}", n),
    }


    // Destructure enums
    match some_number() 
    {
        // Got `Some` variant, match if its value, bound to `n`,
        // is equal to 42.
        Some(numero @ 42) => println!("The Answer: {}!", numero),
        // Match any other number.
        Some(n)      => println!("Not interesting... {}", n),
        // Match anything else (`None` variant).
        // It is important because match is exhaustive and checks all variants
        _            => (),
    }
}
