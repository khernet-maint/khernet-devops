use std::num::ParseIntError;

type ElResultado<T>=Result<T, ParseIntError> ;

fn multiply(first_number_str: &str, second_number_str: &str) -> ElResultado<i32> 
{
    // Until now, unwrap has forced us to nest deeper and deeper when 
    // what we really wanted was to get the variable out. This is exactly the purpose of ?

    // Upon finding an Err, there are two valid actions to take:
    //  panic! which we already decided to try to avoid if possible
    //  return because an Err means it cannot be handled

    // ? is almost1 exactly equivalent to an unwrap which returns instead of `panic` on `Err`.

    let first_number = first_number_str.parse::<i32>()?;
    let second_number = second_number_str.parse::<i32>()?;

    Ok(first_number * second_number)
}

fn print(result: ElResultado<i32>) 
{
    match result 
    {
        Ok(n)  => println!("n is {}", n),
        Err(e) => println!("the error is: {}", e),
    }
}

fn main() 
{
    print(multiply("10", "2"));
    print(multiply("t", "2"));
}
