use std::num::ParseIntError;

// If an error occurs within the main function it will return an error code
// and print a debug representation of the error (using the Debug trait). 
fn main() -> Result<(), ParseIntError> 
{
    let number_str = "10r";
    let number = match number_str.parse::<i32>() 
    {
        Ok(number)  => number,
        //Err(e) => return Some(e),
        //               ^^^^^^^ expected enum `std::result::Result`, found enum `Option`

        //Err(e)=> return Err(e),

        //Err(_)=> return Err(3),
        //                    ^ expected struct `ParseIntError`, found integer

        Err(e)=>return Err(e), // e is a **struct** of type `ParseIntError`
    };
    
    println!("{}", number);
    // Result:
    // Error: ParseIntError { kind: InvalidDigit }
    Ok(())
}
