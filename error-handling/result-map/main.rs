use std::num::ParseIntError;

// With the return type rewritten, we use pattern matching without `unwrap()`.
fn multiply(first_number_str: &str, second_number_str: &str) -> Result<i32, ParseIntError> 
{
    match first_number_str.parse::<i32>() 
    {
        Ok(first_number)  => 
        {
            match second_number_str.parse::<i32>() 
            {
                Ok(second_number)  => 
                {
                    Ok(first_number * second_number)
                },
                Err(e) => Err(e),
            }
        },
        Err(e) => Err(e),
    }
}

fn print(result: Result<i32, ParseIntError>) 
{
    match result 
    {
        Ok(n)  => println!("n is {}", n),
        Err(e) => println!("I found this Error: {}", e),
    }
}

// As with `Option`, we can use combinators such as `map()`.
// This function is otherwise identical to the one above and reads:
// Modify n if the value is valid, otherwise pass on the error.
fn multiply_v2(first_number_str: &str, second_number_str: &str) -> Result<i32, ParseIntError> 
{
    // First call to `parse` returns  Result<i32, ParseIntError> if `first_number_str` is not valid
    // otherwise goes to `and_then`
    // Luckily, Option's map, and_then, and many other combinators are also implemented for `Result`.
    first_number_str.parse::<i32>().and_then(|first_number| 
        {
            second_number_str.parse::<i32>().map(|second_number| first_number * second_number)
        })
}

fn print_v2(result: Result<i32, ParseIntError>) {
    match result {
        Ok(n)  => println!("n is {}", n),
        Err(e) => println!("Error: {}", e),
    }
}

fn main() 
{
    // This still presents a reasonable answer.
    let twenty = multiply("10", "2");
    print(twenty);

    // The following now provides a much more helpful error message.
    let tt = multiply("t", "2");
    print(tt);
    // If above line does not fail and tne next line is executed

    let another_value=multiply_v2("3","23");
    print_v2(another_value);
}
