fn multiply(first_number_str: &str, second_number_str: &str) -> i32 
{
    // It might not always be possible to parse a string into the other type, 
    // so parse() returns a Result indicating possible failure.
    // That is, Result<T, E> could have one of two outcomes:

    //  Ok(T): An element T was found
    //  Err(E): An error was found with element E

    // Let's try using `unwrap()` to get the number out. Will it bite us?
    let first_number = first_number_str.parse::<i32>().unwrap();
    let second_number = second_number_str.parse::<i32>().unwrap();
    first_number * second_number
}

fn main() 
{
    let twenty = multiply("10", "2");
    println!("double is {}", twenty);

    let tt = multiply("t", "2");
    println!("double is {}", tt);
}
