﻿// If this line is commented our tne next warning is raised
// unreachable statement
#![allow(unreachable_code)]

fn main() 
{
    'outer: loop 
    {
        println!("Entered the outer loop");

        'inner: loop 
        {
            println!("Entered the inner loop");

            // This would break only the inner loop
            // and a infinite loop will be executed
            // break;

            // This breaks the outer loop
            break 'outer;
        }

        println!("This point will never be reached");
    }

    println!("Exited the outer loop");
}
