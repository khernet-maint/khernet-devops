use std::fmt::Debug;

// Use this command to compile a rust library
// rustc --crate-type=lib lib.rs

fn print_it( input: impl Debug + 'static )
{
    println!( "'static value passed in is: {:?}", input );
}

static global:i32=9;

fn use_it()
{
    // i is owned and contains no references, thus it's 'static:
    let i = 5;
    print_it(i);

    // oops, &i only has the lifetime defined by the scope of
    // use_it(), so it's not 'static:
    //print_it(&i);

    print_it(&global);
}
