use std::iter;
use std::vec::IntoIter;

// This function combines two `Vec<i32>` and returns an iterator over it.
// Look how complicated its return type is!
fn combine_vecs_explicit_return_type(
    v: Vec<i32>,
    u: Vec<i32>,
) -> iter::Cycle<iter::Chain<IntoIter<i32>, IntoIter<i32>>> 
{
    v.into_iter().chain(u.into_iter()).cycle()
}

// This is the exact same function, but its return type uses `impl Trait`.
// Look how much simpler it is!
fn combine_vecs(
    v: Vec<i32>,
    u: Vec<i32>,
) -> impl Iterator<Item=i32> 
{
    v.into_iter().chain(u.into_iter()).cycle()
}

// More importantly, some Rust types can't be written out. 
// For example, every closure has its own unnamed concrete type. 
// Before impl Trait syntax, you had to allocate on the heap in order to return a closure.
// But now you can do it all statically, like this:
// Returns a function that adds `y` to its input
fn make_adder_function(y: i32) -> impl Fn(i32) -> i32 
{
    let closure = move |x: i32| { x + y };
    closure
}

fn double_positives<'a>(numbers: &'a Vec<i32>) -> impl Iterator<Item = i32> + 'a 
//fn double_positives<'a>(numbers: &'a Vec<i32>) -> impl Iterator<Item > + 'a  // fails
//fn double_positives<'a>(numbers: &'a Vec<i32>) -> impl Iterator<i32> + 'a  // fails
{
    numbers
        .iter()
        .filter(|x| x > &&0)
        .map(|x| x * 2)
}

fn main() 
{
    let v1 = vec![1, 2, 3];
    let v2 = vec![4, 5];
    let mut v3 = combine_vecs(v1, v2);
    assert_eq!(Some(1), v3.next());
    assert_eq!(Some(2), v3.next());
    assert_eq!(Some(3), v3.next());
    assert_eq!(Some(4), v3.next());
    assert_eq!(Some(5), v3.next());
    println!("all done");

    // `v1` and `v2` vectors cannot be used again because those where moved
    // so let's create new ones:

    let v4 = vec![9, 21, 0];
    let v5 = vec![5, 8];
    let mut combined_vector=combine_vecs_explicit_return_type(v4,v5);
    assert_eq!(Some(9), combined_vector.next());
    assert_eq!(Some(21), combined_vector.next());
    assert_eq!(Some(0), combined_vector.next());
    assert_eq!(Some(5), combined_vector.next());
    assert_eq!(Some(8), combined_vector.next());
    println!("all done with combined_vector");

    let plus_one = make_adder_function(1);
    assert_eq!(plus_one(2), 3);

    let v5 = vec![9, 21, 0];

    // note: `#[warn(unused_must_use)]` on by default
    // note: iterators are lazy and do nothing unless consumed
    let result_vec= double_positives(&v5);

    println!("Total length: {}",result_vec.count());
    // Result 2

    // Error:
    // `result_vec` moved due to this method call: `count()`
    // for i in result_vec.into_iter()
    // {
    //     println!("> {}",i);
    // }
}
