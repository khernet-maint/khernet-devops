﻿use std::fmt;

struct Circle 
{
    radius: i32
}

// Rather than doing so directly, you should implement the fmt::Display trait 
// which automatically provides ToString
impl fmt::Display for Circle 
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        write!(f, "Circle of radius {}", self.radius)
    }
}

fn main() 
{
    let circle = Circle { radius: 6 };
    println!("{}", circle.to_string());

    // Use type inference to parse number
    let parsed: i32 = "5".parse().unwrap();

    // Use turbofish syntax to parse number
    let turbo_parsed = "10".parse::<i32>().unwrap();

    let sum = parsed + turbo_parsed;
    println!("Sum: {:?}", sum);
}
