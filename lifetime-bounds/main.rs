use std::fmt::Debug; // Trait to bound with.

#[derive(Debug)]
struct Ref<'a, T: 'a>(&'a T);
// `Ref` contains a reference to a generic type `T` that has
// an unknown lifetime `'a`. `T` is bounded such that any
// *references* in `T` must outlive `'a`. Additionally, the lifetime
// of `Ref` may not exceed `'a`.

// A generic function which prints using the `Debug` trait.
fn print<T>(t: T) where
    T: Debug
{
    println!("`print`: t is {:?}", t);
}

// Here a reference to `T` is taken where `T` implements
// `Debug` and all *references* in `T` outlive `'a`. In
// addition, `'a` must outlive the function.
fn print_ref<'a, T>(t: &'a T) where
    T: Debug + 'a 
{
    println!("`print_ref`: t is {:?}", t);
}

// This functions fails with
// ^^ undeclared lifetime
// fn print_ref2<'b, T>(t: &'a T) where
//     T: Debug + 'b 
// {
//     println!("`print_ref`: t is {:?}", t);
// }

fn print_ref3<'a, T>(t: &'a T) where
    T: Debug 
{
    println!("`print_ref3`: t is {:?}", t);
}


fn main() 
{
    let x = 7;
    let ref_x = Ref(&x);

    print_ref(&ref_x);
    print(ref_x);
    // Comment out the above line for next line to work
    // This is due that a variable `ref_x` cannot be used while borrowed `&ref_x`
    
    // This line fails with
    // error[E0382]: borrow of moved value: `ref_x`
    //print_ref3(&ref_x);
    // Comment out the above line to fix
}
