﻿// Similarly `mod inaccessible` and `mod nested` will locate the `nested.rs`
// and `inaccessible.rs` files and insert them here under their respective
// modules

// This line will look for a file named 'inaccessible.rs' or 'inaccessible/mod.rs'
mod inaccessible;
pub mod nested;

pub fn function() 
{
    println!("called `my::function()`");
}

fn private_function() 
{
    println!("called `my::private_function()`");
}

pub fn indirect_access() 
{
    print!("called `my::indirect_access()`, that\n> ");

    private_function();
}
