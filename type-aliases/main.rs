﻿
enum VeryVerboseEnumOfThingsToDoWithNumbers 
{
    Add,
    Subtract,
}

// Creates a type alias
// This might be useful if the enum's name is too long or too generic, and you want to rename it.
type Operations = VeryVerboseEnumOfThingsToDoWithNumbers;

// VeryVerboseEnumOfThingsToDoWithNumbers enum must exists before using `impl`
impl VeryVerboseEnumOfThingsToDoWithNumbers 
{
    fn run(&self, x: i32, y: i32) -> i32 
    {
        match self 
        {
            Self::Add => x + y,
            Self::Subtract => x - y,
        }
    }
}


fn main() 
{
    // We can refer to each variant via its alias, not its long and inconvenient
    // name.
    let x = Operations::Add;
    let y=Operations::Subtract;

    // The two lines will fail because trait `Debug` is not implemented
    //println!("Variable x={:?}",x);
    //println!("Variable y={:?}",y);

    // The two lines will fail because trait `std::fmt::Display` is not implemented
    //println!("Variable x={}",x);
    //println!("Variable y={}",y);
}
