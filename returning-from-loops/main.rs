﻿fn main() 
{
    let mut counter = 0;

    let result = loop {
        counter += 1;

        if counter == 10 {

            // The result of `counter * 2` will be returned by loop
            break counter * 2;
        }
    };

    assert_eq!(result, 20);
}
