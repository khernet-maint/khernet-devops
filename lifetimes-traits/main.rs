// A struct with annotation of lifetimes.
#[derive(Debug)]
 struct Borrowed<'a> 
 {
     x: &'a i32,
 }

// Annotate lifetimes to impl.
// Default is a native trait
impl<'a> Default for Borrowed<'a> 
{
    fn default() -> Self 
    {
        let va:&'a i32=&33;
        Self 
        {
            //x: &10,
            x: va,
            //x: &10 // This works too.
        }
    }
}

fn main() 
{
    let b: Borrowed = Default::default();
    println!("b is {:?}", b);
}
