struct Droppable 
{
    name: &'static str,
}

// This trivial implementation of `drop` adds a print to console.
impl Drop for Droppable 
{
    fn drop(&mut self) {
        println!("> Dropping {}", self.name);
    }
}

fn main() 
{
    let _a = Droppable { name: "a" };

    // block A
    {
        let _b = Droppable { name: "b" };

        // block B
        {
            let _c = Droppable { name: "c" };
            let _d = Droppable { name: "d" };

            println!("Exiting block B");
        }
        println!("Just exited block B");

        println!("Exiting block A");
    }
    println!("Just exited block A");

    // Variable can be manually dropped using the `drop` function
    drop(_a);
    // TODO ^ Try commenting this line


    // With uncommented line
    // Exiting block B
    // > Dropping d
    // > Dropping c
    // Just exited block B
    // Exiting block A
    // > Dropping b
    // Just exited block A
    // > Dropping a
    // end of the main function
    

    // With commented line
    // Exiting block B
    // > Dropping d
    // > Dropping c
    // Just exited block B
    // Exiting block A
    // > Dropping b
    // Just exited block A
    // end of the main function
    // > Dropping a

    println!("end of the main function");

    // `_a` *won't* be `drop`ed again here, because it already has been
    // (manually) `drop`ed
}
