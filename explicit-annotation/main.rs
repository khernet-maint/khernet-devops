// `print_refs` takes two references to `i32` which have different
// lifetimes `'a` and `'b`. These two lifetimes must both be at
// least as long as the function `print_refs`.
fn print_refs<'luis, 'b>(x: &'luis i32, y: &'b i32) 
{
    println!("x is {} and y is {}", x, y);
    println!("again x is {} and y is {}", x, y);

    let _xcopy: &'luis i32=&x;

    println!("_xcopy={}",_xcopy);
    println!("other _xcopy={}",*_xcopy);
}

// This lifetime syntax indicates that the lifetime of failed_borrow may not exceed that of 'a
// A function which takes no arguments, but has a lifetime parameter `'a`.
fn failed_borrow<'a>() 
{
    let _x = 12;

    let _number=_x;
    let _number2=&_x;

    println!("_number2={}",_number2);

    // ERROR: `_x` does not live long enough because `failed_borrow` lives longer
    let y: &'a i32 = &_x;
    // Attempting to use the lifetime `'a` as an explicit type annotation 
    // inside the function will fail because the lifetime of `&_x` is shorter
    // than that of `y`. A short lifetime cannot be coerced into a longer one.
}
// `_x` dropped here while still borrowed


fn main() 
{
    // Create variables to be borrowed below.
    let (four, nine) = (4, 9);
    
    // Borrows (`&`) of both variables are passed into the function.
    print_refs(&four, &nine);
    print_refs(&four, &nine);
    // Any input which is borrowed must outlive the borrower. 
    // In other words, the lifetime of `four` and `nine` must 
    // be longer than that of `print_refs`.
    
    failed_borrow();
    // `failed_borrow` contains no references to force `'a` to be 
    // longer than the lifetime of the function, but `'a` is longer.
    // Because the lifetime is never constrained, it defaults to `'static`.
}
