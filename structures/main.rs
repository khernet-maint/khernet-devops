﻿#[derive(Debug)]
struct Person 
{
    name: String,
    age: u8,
}

// A unit struct
struct Unit;

// A tuple struct
struct Pair(i32, f32);

// A struct with two fields
struct Point 
{
    x: f32,
    y: f32,
}

// Structs can be reused as fields of another struct
#[allow(dead_code)]
struct Rectangle 
{
    // A rectangle can be specified by where the top left and bottom right
    // corners are in space.
    top_left: Point,
    bottom_right: Point,
}

fn rect_area(rec: Rectangle)->f32
{
    let base=rec.bottom_right.x- rec.top_left.x;
    let height=rec.top_left.y-rec.bottom_right.y;

    //println!("Area: {}",base*height);
    base*height
}

fn square(initialPoint: Point, height: f32, width: f32)->Rectangle
{
    let topLeft=Point{x:initialPoint.x, y:initialPoint.y+height};
    let bottomRight=Point{x:initialPoint.x+width, y:initialPoint.y};

    Rectangle{top_left:topLeft, bottom_right:bottomRight}
}


fn main() 
{
    // Create struct with field init shorthand
    let name = String::from("Peter");
    let age = 27;
    let peter = Person { name, age };

    // Print debug struct
    println!("{:?}", peter);


    // Instantiate a `Point`
    let point: Point = Point { x: 10.3, y: 0.4 };

    //Instantiate without specifying type
    let otherPoint = Point { x: 10.3, y: 0.4 };

    // Access the fields of the point
    println!("point coordinates: ({}, {})", point.x, point.y);

    // Make a new point by using struct update syntax to use the fields of our
    // other one
    let bottom_right = Point { x: 5.2, ..point };

    // the base struct must always be the last field
    // so this line will fail
    // let otherBottom_right = Point { ..point, 23 };

    // `bottom_right.y` will be the same as `point.y` because we used that field
    // from `point`
    println!("second point: ({}, {})", bottom_right.x, bottom_right.y);

    // Destructure the point using a `let` binding
    let Point { x: top_edge, y: left_edge } = point;

    // New variable names are: lado_superior and lado_izquierdo which can be used later
    // regardless
    let Point { x: lado_superior, y: lado_izquierdo } = point;

    println!("My new variable lado_superior = {}",lado_superior);

    let _rectangle = Rectangle {
        // struct instantiation is an expression too
        top_left: Point { x: left_edge, y: top_edge },
        bottom_right: bottom_right,
    };

    // Instantiate a unit struct
    let _unit = Unit;

    // Instantiate a tuple struct
    let pair = Pair(1, 0.1);

    // Access the fields of a tuple struct
    println!("pair contains {:?} and {:?}", pair.0, pair.1);

    // Destructure a tuple struct
    let Pair(integer, decimal) = pair;

    println!("pair contains {:?} and {:?}", integer, decimal);

    println!("Area = {:?}", rect_area(_rectangle));

    let initialPoint=Point{x:0f32,y:0f32};
    let height=54f32;
    let width=12f32;

    let newRec=square(initialPoint,height,width);
    println!("Square x:\n({:?}) ({:?})",newRec.top_left.x,newRec.bottom_right.x);
    println!("Square y:\n({:?}) ({:?})",newRec.top_left.y,newRec.bottom_right.y);
}
