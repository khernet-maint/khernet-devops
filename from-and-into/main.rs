﻿use std::convert::From;

#[derive(Debug)]
struct Number 
{
    value: i32,
}

// The From trait allows for a type to define how to create itself from another type, 
// hence providing a very simple mechanism for converting between several types. 
impl From<i32> for Number 
{
    fn from(item: i32) -> Self 
    {
       Number { value: item }
    }

    // `desde` is not a member of trait `From`
    // fn desde(item: i32) -> Self 
    // {
    //     Number { value: item }
    // }
}

fn main() 
{
    let num = Number::from(30);
    println!("My number is {:?}", num);

    let int = 5;
    // Try removing the type declaration
    let num: Number= int.into();
    // The Into trait is simply the reciprocal of the From trait. 
    // That is, if you have implemented the From trait for your type, Into will call it when necessary.

    // this line will fail because `num` does not have a type
    //let num = int.into();

    println!("My second number is {:?}", num);
}
