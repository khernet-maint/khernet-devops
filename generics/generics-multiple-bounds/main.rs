﻿use std::fmt::{Debug, Display};

fn compare_prints<T: Debug + Display>(t: &T) 
{
    println!("Debug: `{:?}`", t);
    println!("Display: `{}`", t);
}

fn compare_types<T: Debug, U: Debug>(t: &T, u: &U) 
{
    println!("t: `{:?}`", t);
    println!("u: `{:?}`", u);
}

fn main() 
{
    // Strings already implements `Debug` and `Display` traits
    let string = "words";

    // Arrays already implements `Debug` trait
    let array = [1, 2, 3];
    
    // `vec` already implements `Debug` trait
    let vec = vec![1, 2, 3];

    compare_prints(&string);
    //compare_prints(&array);
    // TODO ^ Try uncommenting this.
    // Result:
    // error[E0277]: `[{integer}; 3]` doesn't implement `std::fmt::Display`

    compare_types(&array, &vec);
}
