﻿struct Container(i32, i32);

// The use of "Associated types" improves the overall readability of code 
// by moving inner types locally into a trait as output types. 

// A trait which checks if 2 items are stored inside of container.
// Also retrieves first or last value.
trait Contains {
    // Define generic types here which methods will be able to utilize.
    type A;
    type B;

    fn contains(&self, _: &Self::A, _: &Self::B) -> bool;
    fn first(&self) -> i32;
    fn last(&self) -> i32;
}

impl Contains for Container {
    // Specify what types `A` and `B` are. If the `input` type
    // is `Container(i32, i32)`, the `output` types are determined
    // as `i32` and `i32`.
    type A = i32;
    type B = i32;

    // `&Self::A` and `&Self::B` are also valid here.
    fn contains(&self, number_1: &i32, number_2: &i32) -> bool {
        (&self.0 == number_1) && (&self.1 == number_2)
    }
    // Grab the first number.
    fn first(&self) -> i32 { self.0 }

    // Grab the last number.
    fn last(&self) -> i32 { self.1 }
}

fn difference<C: Contains>(container: &C) -> i32 {
    container.last() - container.first()
}


struct MyCollection
{
    first_number:i32,
    last_number:i32
}

trait Finder
{
    type A;
    type B;

    fn Contains(&self,_:&Self::A,_:&Self::B)->bool;
    fn First(&self)->i32;
    fn Last(&self)->i32;
}

impl Finder for MyCollection
{
    type A=i32;
    type B=i32;

    fn Contains(&self,number_1:&i32,number_2:&i32)->bool
    {
        (&self.first_number==number_1)&&(&self.last_number==number_2)
    }

    fn First(&self)->i32
    {
        self.first_number
    }

    fn Last(&self)->i32
    {
        self.last_number
    }
}

fn main() {
    let number_1 = 3;
    let number_2 = 10;

    let container = Container(number_1, number_2);

    println!("Does container contain {} and {}: {}",
        &number_1, &number_2,
        container.contains(&number_1, &number_2));
    println!("First number: {}", container.first());
    println!("Last number: {}", container.last());
    
    println!("The difference is: {}", difference(&container));

    let my_collection=MyCollection{first_number:number_1,last_number:number_2};

    println!("Does my_collection contain {} and {}: {}",
        &number_1, &number_2,
        my_collection.Contains(&number_1, &number_2));
}
