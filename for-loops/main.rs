﻿fn main() 
{
    // `n` will take the values: 1, 2, ..., 100 in each iteration
    // value 1 is included
    // value 101 is excluded
    for n in 1..101 
    {
        if n % 15 == 0 
        {
            println!("fizzbuzz");
        } 
        else if n % 3 == 0 
        {
            println!("fizz");
        } 
        else if n % 5 == 0 
        {
            println!("buzz");
        } 
        else 
        {
            println!("{}", n);
        }
    }


    // `n` will take the values: 1, 2, ..., 100 in each iteration
    // value 1 is included
    // equals `=` makes value 100 to be included
    for n in 1..=100 
    {
        if n % 15 == 0 
        {
            println!("fizzbuzz");
        } 
        else if n % 3 == 0 
        {
            println!("fizz");
        } 
        else if n % 5 == 0 
        {
            println!("buzz");
        } 
        else 
        {
            println!("{}", n);
        }
    }

    // Call iterators

    println!("******ITERATORS*******");
    println!("\n---- iter ----\n");
    call_iterator_iter();

    println!("\n---- into_iter ----\n");
    call_iterator_into_iter();

    println!("\n---- iter_mut ----\n");
    call_iterator_iter_mut();
}

// This borrows each element of the collection through each iteration. 
// Thus leaving the collection untouched and available for reuse after the loop.
fn call_iterator_iter()
{
    let names = vec!["Bob", "Frank", "Ferris"];

    for name in names.iter() 
    {
        match name 
        {
            &"Ferris" => println!("There is a rustacean among us!"),
            _ => println!("Hello {}", name),
        }
    }
}

// This consumes the collection so that on each iteration the exact data is provided. 
// Once the collection has been consumed it is no longer available for reuse as it has been 'moved' within the loop.
fn call_iterator_into_iter()
{
    let names = vec!["Bob", "Frank", "Ferris"];

    for name in names.into_iter() 
    {
        match name 
        {
            "Ferris" => println!("There is a rustacean among us!"),
            _ => println!("Hello {}", name),
        }
    }

    // Error:
    // borrow of moved value: `names`
    // value borrowed here after move
    // println!("Print array after into_iter: {:?}",names);
}

// This mutably borrows each element of the collection, allowing for the collection to be modified in place.
fn call_iterator_iter_mut()
{
    let mut names = vec!["Bob", "Frank", "Ferris"];

    for name in names.iter_mut() 
    {
        *name = match name 
        {
            &mut "Ferris" => "There is a rustacean among us!",
            _ => "Hello",
        }
    }

    // "Ferris" is changed to "There is a rustacean among us!"
    println!("names: {:?}", names);
}