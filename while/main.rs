﻿fn main() 
{
    // A counter variable
    let mut n = 1;

    // Parentheses are not necessary otherwise a warning is raised
    // unnecessary parentheses around `while` condition
    // while n < 101 

    // Loop while `n` is less than 101
    while n < 101
    {
        if n % 15 == 0 
        {
            println!("fizzbuzz");
        } 
        else if n % 3 == 0 
        {
            println!("fizz");
        } 
        else if n % 5 == 0 
        {
            println!("buzz");
        } 
        else 
        {
            println!("{}", n);
        }

        // Increment counter
        n += 1;
    }
}
