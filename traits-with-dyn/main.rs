struct Sheep {}
struct Cow {}

trait Animal 
{
    // Instance method signature
    fn noise(&self) -> &'static str;
}

// Implement the `Animal` trait for `Sheep`.
impl Animal for Sheep 
{
    fn noise(&self) -> &'static str 
    {
        "baaaaah!"
    }
}

// Implement the `Animal` trait for `Cow`.
impl Animal for Cow 
{
    fn noise(&self) -> &'static str 
    {
        "moooooo!"
    }
}

// Instead of returning a trait object directly, our functions return a Box which contains some Animal. 
// A box is just a reference to some memory in the heap. Because a reference has a statically-known size, 
// and the compiler can guarantee it points to a heap-allocated Animal, we can return a trait from our function!

// Returns some struct that implements Animal, but we don't know which one at compile time.
fn random_animal(random_number: f64) -> Box<dyn Animal> 
{
    if random_number < 0.5 
    {
        Box::new(Sheep {})
    } 
    else 
    {
        Box::new(Cow {})
    }
}

// The Rust compiler needs to know how much space every function's return type requires. 
// This means all your functions have to return a concrete type. 
// Unlike other languages, if you have a trait like Animal, you can't write a function that returns Animal,
// because its different implementations will need different amounts of memory.

// warning: trait objects without an explicit `dyn` are deprecated
// fn random_animal_2(random_number: f64) -> Animal // ^^^^^^ doesn't have a size known at compile-time
// {
//     // help: return a boxed trait object instead
//     if random_number < 0.5 
//     {
//         Box::new(Sheep {})// error[E0746]: return type cannot have an unboxed trait object
//     } 
//     else 
//     {
//         Box::new(Cow {})
//     }
// }

fn main() 
{
    let random_number = 0.234;
    let animal = random_animal(random_number);
    println!("You've randomly chosen an animal, and it says {}", animal.noise());
}

