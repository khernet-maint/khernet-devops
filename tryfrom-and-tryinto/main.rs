﻿use std::convert::TryFrom;
use std::convert::TryInto;

#[derive(Debug, PartialEq)]
struct EvenNumber(i32);

impl TryFrom<i32> for EvenNumber 
{
    type Error = ();

    fn try_from(value: i32) -> Result<Self, Self::Error> 
    {
        if value % 2 == 0 
        {
            Ok(EvenNumber(value))
            //okey(EvenNumber(value))
        } 
        else 
        {
            Err(())
        }
    }
}

fn main() 
{
    // TryFrom

    assert_eq!(EvenNumber::try_from(8), Ok(EvenNumber(8)));
    assert_eq!(EvenNumber::try_from(5), Err(()));

    // This line will raise:
    // thread 'main' panicked at 'assertion failed: `(left == right)`
    // assert_eq!(EvenNumber::try_from(5), Ok(EvenNumber(5)));
    // Commet out the above line to continue execution

    let _my_number=EvenNumber::try_from(10);
    let _other_number=EvenNumber::try_from(11);

    println!("My number: {:?}",_my_number);
    println!("My other number: {:?}",_other_number);

    // TryInto

    let result: Result<EvenNumber, ()> = 8i32.try_into();
    assert_eq!(result, Ok(EvenNumber(8)));
    let result: Result<EvenNumber, ()> = 5i32.try_into();
    assert_eq!(result, Err(()));
}
