﻿fn main() 
{
    let pair = (5, -1);
    // TODO ^ Try different values for `pair`

    println!("Tell me about {:?}", pair);
    match pair 
    {
        (x, y) if x == y => println!("These are twins"),
    
        // The ^ `if condition` part is a guard
        // guards are used to filter an arm of match statement
        (x, y) if x + y == 0 => println!("Antimatter, kaboom!"),
    
        (x, _) if x % 2 == 1 => println!("The first one is odd"),

        // error: expected one of `=>`, `if`, or `|`, found keyword `let`
        // (x, _) let r=4; if x % 2 == 1 => println!("The first one is odd"),
    
        _ => println!("No correlation..."),
    }
}
